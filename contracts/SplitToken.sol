pragma solidity >=0.4.21 <0.6.0;
import "../erc20/IERC20.sol";
import "../erc20/SafeERC20.sol";
import "../erc20/ERC20Impl.sol";
import "../utils/SafeMath.sol";


contract SplitToken{
  using SafeERC20 for IERC20;
  using SafeMath for uint256;

  address public target_token;
  address public recv1_addr;
  address public recv2_addr;

  constructor(address _target_token, address _r1_addr, address _r2_addr) public{
    target_token = _target_token;
    recv1_addr = _r1_addr;
    recv2_addr = _r2_addr;
  }

  function split() public{
    uint256 b = IERC20(target_token).balanceOf(address(this));
    uint256 r1 = b.safeDiv(2);
    IERC20(target_token).safeTransfer(recv1_addr, r1);
    IERC20(target_token).safeTransfer(recv2_addr, b.safeSub(r1));
  }

}
