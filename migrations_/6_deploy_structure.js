const {DHelper, StepRecorder} = require("./util.js");
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const AddressArray = artifacts.require("AddressArray");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const PDispatcher = artifacts.require("PDispatcher");
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const PMintBurn = artifacts.require("PMintBurn");
const PMBParams = artifacts.require("PMBParams");
const PNaivePriceOracle = artifacts.require("PNaivePriceOracle");
const PLiquidateAgent = artifacts.require("PLiquidateAgent");
const ERC20Staking = artifacts.require("ERC20Staking");

async function performMigration(deployer, network, accounts, dhelper) {
  tbank_factory = await dhelper.readOrCreateContract(TokenBankV2Factory)
  token_factory = await dhelper.readOrCreateContract(ERC20TokenFactory, [AddressArray]);
  tlist_factory = await dhelper.readOrCreateContract(TrustListFactory, [AddressArray])

  sr =  new StepRecorder(network, "structure");
  tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  tlist = await TrustList.at(tx.logs[0].args.addr);
  await tlist.add_trusted(accounts[0]);
  tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
      0, "Plutos", 18, "PLUT", true, tlist.address)
  plut_token = await ERC20Token.at(tx.logs[0].args._cloneToken);
  sr.write("plut_token_trustlist", tlist.address);
  sr.write("plut_token", plut_token.address);

  tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  pusd_tlist = await TrustList.at(tx.logs[0].args.addr);
  tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
      0, "Plutos USD", 18, "pUSD", true, pusd_tlist.address)
  stable_token = await ERC20Token.at(tx.logs[0].args._cloneToken);
  sr.write("pusd_token_trustlist", tlist.address);
  sr.write("pusd_token", stable_token.address);


  dispatcher = await dhelper.readOrCreateContract(PDispatcher, []);
  sr.write("dispatcher", dispatcher.address);


  tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  pool_tlist = await TrustList.at(tx.logs[0].args.addr);
  tx = await tbank_factory.newTokenBankV2("plutos pool", pool_tlist.address);
  pool = await TokenBankV2.at(tx.logs[0].args.addr);
  sr.write("pool", pool.address);

  mb = await dhelper.readOrCreateContract(PMintBurn, [SafeMath, SafeERC20], plut_token.address, stable_token.address, pool.address, dispatcher.address);
  await pusd_tlist.add_trusted(mb.address);
  await pool_tlist.add_trusted(mb.address);
  sr.write("mint_burn", mb.address);

  param = await dhelper.readOrCreateContract(PMBParams, []);
  sr.write("param", param.address);
  price = await dhelper.readOrCreateContract(PNaivePriceOracle, []);
  sr.write("price", price.address);
  liquidate = await dhelper.readOrCreateContract(PLiquidateAgent, [SafeMath, SafeERC20], plut_token.address, pool.address, stable_token.address, dispatcher.address);
  await liquidate.changeCaller(mb.address);
  sr.write("liquidate", liquidate.address);

  await pusd_tlist.add_trusted(liquidate.address);
  await pool_tlist.add_trusted(liquidate.address);

  await dispatcher.resetTarget(await mb.param_key(), param.address);
  await dispatcher.resetTarget(await mb.price_key(), price.address);
  await dispatcher.resetTarget(await mb.liquidate_key(), liquidate.address);

  tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  tlist = await TrustList.at(tx.logs[0].args.addr);
  tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
      0, "Plutos Staking Token", 18, "xPLUT", true, tlist.address)
  xplut_token = await ERC20Token.at(tx.logs[0].args._cloneToken);
  sr.write("xplut_token_trustlist", tlist.address);
  sr.write("xplut_token", xplut_token.address);
  staking = await dhelper.readOrCreateContract(ERC20Staking, [SafeMath, SafeERC20], plut_token.address, xplut_token.address);

  await tlist.add_trusted(staking.address) 
  
  sr.write("staking", staking.address);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
