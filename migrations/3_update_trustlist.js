const PMintBurn = artifacts.require("PMintBurn");
const {DHelper, StepRecorder} = require("./util.js");
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const TrustList = artifacts.require("TrustList");
const PLiquidateAgent = artifacts.require("PLiquidateAgent");


async function performMigration(deployer, network, accounts, dhelper) {
    pusd_token_trustlist = "0x9eC17d407c08501e1627cDb4D38Fba374Aa6A4Bb"

    pusd_tlist = await TrustList.at(pusd_token_trustlist);
    await pusd_tlist.add_trusted("0xf5FA30b938C3A3506e738BCD842497D5ce9A220F");
}


module.exports = function(deployer, network, accounts){
    deployer
      .then(function() {
        return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
      })
      .catch(error => {
        console.log(error)
        process.exit(1)
      })
  };
