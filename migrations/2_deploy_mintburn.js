const PMintBurn = artifacts.require("PMintBurn");
const {DHelper, StepRecorder} = require("./util.js");
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const TrustList = artifacts.require("TrustList");
const PLiquidateAgent = artifacts.require("PLiquidateAgent");


async function performMigration(deployer, network, accounts, dhelper) {
    sr =  new StepRecorder(network, "structure");

    plut_token = "0xe7e4A857Ff7F1360C654147d29C38719bcA91A87"
    stable_token = "0x3dfe621515206b583D5985Bb6F891E5F43868e73"
    pool = "0x749183DCf218A3896D1b65bd2cF487E36a5e4947"
    dispatcher = "0x7ECA87F8737870B6F561a1866e972FF3720299Ed"
    pusd_token_trustlist = "0x36f9D3102B4D907bD1123dfcb327A1726519B704"
    pool_trustlist_addr = "0x17427aa596cde482a299bcac61655c973b65ec24"
    owner = "0xF47619A9b2116496580eecb3c3b9747610ae4900"

    mb = await dhelper.readOrCreateContract(PMintBurn, [SafeMath, SafeERC20], plut_token, stable_token, pool, dispatcher);

    old_mb = "0xC4dF5B89D58eec8305e9bd2e081CefeC36dF8DC5"

    pusd_tlist = await TrustList.at(pusd_token_trustlist);
    pool_tlist = await TrustList.at(pool_trustlist_addr);
    await pusd_tlist.add_trusted(mb.address);
    await pool_tlist.add_trusted(mb.address);
    await pusd_tlist.remove_trusted(old_mb);
    await pool_tlist.remove_trusted(old_mb);
    sr.write("mint_burn", mb.address);

    liquidate_addr = "0xF2cFd12925e071779152809AD22Ca3c7F9a5B9c7"

    liquidate = await PLiquidateAgent.at(liquidate_addr)
    await liquidate.changeCaller(mb.address);

    await mb.transferOwnership(owner);
}


module.exports = function(deployer, network, accounts){
    deployer
      .then(function() {
        return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
      })
      .catch(error => {
        console.log(error)
        process.exit(1)
      })
  };
