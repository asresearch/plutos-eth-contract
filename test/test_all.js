const { expect } = require('chai');
const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { keccak256, bufferToHex, toBuffer } = require('ethereumjs-util');
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");

//const getBlockNumber = require('./blockNumber')(web3)
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const PDispatcher = artifacts.require("PDispatcher");
const PMintBurn = artifacts.require("PMintBurn");
const PMBParams = artifacts.require("PMBParams");
const PNaivePriceOracle = artifacts.require("PNaivePriceOracle");
const PLiquidateAgent = artifacts.require("PLiquidateAgent");
const ERC20Staking = artifacts.require("ERC20Staking");

function keccak256_enpacked(addr) {
  k = web3.utils.soliditySha3({t:'string', v:'plutos'},{t:'address', v:addr});
  return toBuffer(k);
}
contract("Test Plut", (accounts)=>{
  context("init", ()=>{
    let price = {}
    let mb = {}
    let param = {}
    let plut = {}
    let xplut = {}
    let pusd = {}
    let i = 0
    let start_block = 0;
    it("init", async function(){
      mb = await PMintBurn.deployed();
      price = await PNaivePriceOracle.deployed();
      dispatcher = await PDispatcher.deployed();
      param = await PMBParams.deployed();

      plut = await ERC20Token.at(await mb.target_token());
      pusd = await ERC20Token.at(await mb.stable_token());

      await plut.generateTokens(accounts[0], '100000000000000000000000000')
      await plut.generateTokens(accounts[1], "100000000000000000000000000")
      await plut.generateTokens(accounts[2], "100000000000000000000000000")
      await plut.generateTokens(accounts[3], "100000000000000000000000000")
      await price.setPrice('1000000000000000000') //price set to 1
      await param.changeMortgageRatio(2000000)
    })

    it('deposit/borrow/repay/withdraw', async function(){
      await plut.approve(mb.address, '10000000000000000000000')
      await mb.deposit('100000000000000000000', {from:accounts[0]})
      await mb.borrow('50000000000000000000', {from:accounts[0]})
      m = await pusd.balanceOf(accounts[0])
      expect(m.toString()).to.equal('50000000000000000000')

      await expectRevert(mb.borrow(1, {from:accounts[0]}), "no left quota")

      await mb.repay('10000000000000000000')
      m = await pusd.balanceOf(accounts[0])
      expect(m.toString()).to.equal('40000000000000000000')

      await mb.withdraw('20000000000000000000')
      await expectRevert(mb.withdraw(1), "claim too much")

      await mb.repay('40000000000000000000')

      await mb.withdraw('80000000000000000000')

      m = await pusd.balanceOf(accounts[0])
      expect(m.toNumber()).to.equal(0);
      m = await plut.balanceOf(accounts[0])
      expect(m.toString()).to.equal('100000000000000000000000000')
    })

    it('liquidate', async function(){
      await plut.approve(mb.address, '1000000000000000000000000', {from:accounts[1]})
      await plut.approve(mb.address, 0, {from:accounts[0]})
      await plut.approve(mb.address, '1000000000000000000000000', {from:accounts[0]})
      await plut.approve(mb.address, '1000000000000000000000000', {from:accounts[2]})
      await mb.deposit(3000);
      await mb.borrow(1000);
      await expectRevert(mb.liquidate(keccak256_enpacked(accounts[0]), 1, {from:accounts[1]}), "mortgage ratio is high, cannot liquidate")
      await expectRevert(mb.liquidate(keccak256_enpacked(accounts[2]), 1, {from:accounts[1]}), "hash not exist")
      await price.setPrice('500000000000000000')

      console.log("b2: ", (await plut.balanceOf(accounts[2])).toString())

      await mb.deposit(1000000, {from:accounts[2]});
      await mb.borrow(10000, {from:accounts[2]});
      await mb.liquidate(keccak256_enpacked(accounts[0]), 3000, {from:accounts[2]})

      m = await pusd.balanceOf(accounts[2])
      expect(m.toNumber()).to.equal(9000);
      m = await plut.balanceOf(accounts[2])
      expect(m.toString()).to.equal('99999999999999999999003000')
      b = await mb.is_liquidatable(keccak256_enpacked(accounts[0]))
      expect(b).to.equal(false)
    })

    it('staking', async function(){
      es = await ERC20Staking.deployed()
      xplutaddr = await es.lp_token()
      xplut = await ERC20Token.at(xplutaddr)
      await plut.destroyTokens(accounts[0], await plut.balanceOf(accounts[0]))
      await plut.destroyTokens(accounts[1], await plut.balanceOf(accounts[1]))
      await plut.generateTokens(accounts[0], '100000000000000000000000000')
      await plut.generateTokens(accounts[1], "100000000000000000000000000")
      await plut.approve(es.address, '1000000000000000000000000', {from:accounts[1]})
      await plut.approve(es.address, '1000000000000000000000000', {from:accounts[0]})

      await es.stake('1000000000000000000000')
      l0 = (await xplut.balanceOf(accounts[0])).toString()
      expect(l0).to.equal('1000000000000000000000')

      await es.claim('1000000000000000000000')
      l0 = (await xplut.balanceOf(accounts[0])).toNumber()
      expect(l0).to.equal(0)

      await es.stake('1000000000000000000000')
      await plut.generateTokens(es.address, '1000000000000000000000')
      await es.claim('1000000000000000000000')
      b0 = (await plut.balanceOf(accounts[0])).toString()
      expect(b0).to.equal('100001000000000000000000000')

      await es.stake('1000000000000000000000')
      await plut.generateTokens(es.address, '1000000000000000000000')
      p = (await es.getPricePerFullShare()).toString()
      expect(p).to.equal('2000000000000000000')

      console.log("pool plut: ", (await plut.balanceOf(es.address)).toString())
      console.log("total xplut: ", (await xplut.totalSupply()).toString())

      await es.stake('2000000000000000000000', {from:accounts[1]})
      l1 = (await xplut.balanceOf(accounts[1])).toString()
      expect(l1).to.equal('1000000000000000000000')

      await plut.generateTokens(es.address, '1000000000000000000000')
      console.log("a1 plut: ", (await plut.balanceOf(accounts[1])).toString())
      await es.claim('500000000000000000000',{from:accounts[1]})

      b1 = (await plut.balanceOf(accounts[1])).toString()
     
      expect(b1).to.equal('99999250000000000000000000')//-2000+1250
    })
    it('deposit and borrow', async function(){
      await plut.approve(mb.address, '10000000000000000000000', {from:accounts[3]})
      await price.setPrice('1000000000000000000')
      await mb.deposit_and_borrow('100000000000000000000', {from:accounts[3]})
      m = await pusd.balanceOf(accounts[3])
      expect(m.toString()).to.equal('25000000000000000000')
      await mb.deposit_and_borrow('200000000000000000000', {from:accounts[3]})
      m = await pusd.balanceOf(accounts[3])
      expect(m.toString()).to.equal('75000000000000000000')
      await mb.borrow('75000000000000000000', {from:accounts[3]})
      m = await pusd.balanceOf(accounts[3])
      expect(m.toString()).to.equal('150000000000000000000')
      await price.setPrice('10000000000000000')
      await expectRevert(mb.deposit_and_borrow('100000000000000000000', {from:accounts[3]}), "no left quota")
    })

  })
})
