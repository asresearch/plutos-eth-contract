# plutos-eth-contract

1. Clone eth-contracts, which is the code base.

  `git clone https://gitlab.com/asresearch/eth-contracts.git`

2. Put this into eth-contracts/contracts/core

```
  cd eth-contracts/contracts
  ln -s PLUTOS-ETH-CONTRACT/contracts core
  cd ..
  ln -s PLUTOS-ETH-CONTRACT/migrations migrations
  ln -s PLUTOS-ETH-CONTRACT/test test
```

